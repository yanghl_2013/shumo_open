#!/usr/bin/env python
# -*- coding: UTF-8 -*-
'''
Created on 2013-7-11d

@author: yanghl
 r=python('D:\workspace\shumo\guanliandu.py') matlab 调用
'''
import numpy as np 

class check(object):
    
    def nomal(self,data):
        #初值归一化
        for i in data:
            one = i[0]
            for k in range(len(i)):
                i[k]=i[k]*1.0/one
        return data
    
    def guanlian(self,raw_data,rou):
        #关联度检测
        raw_data = self.nomal(raw_data)
        len_i = len(raw_data)
        delta = [0 for i in range(len_i-1)]
        for i in range(1,len_i):
            x0=raw_data[0]
            xi=raw_data[i]
            len_k = len(x0)
            deltak=[0 for n in range(len_k)]
            for k in range(len_k):
                deltak[k]=abs(x0[k]-xi[k])
            delta[i-1]=deltak
        min_ik = min([min(i) for i in delta])
        max_ik = max([max(i) for i in delta])
#         print min_ik,max_ik
        
        r = []
        for i in delta:
            ksai = [(min_ik+rou*max_ik)*1.0/(i[k]+rou*max_ik) for k in range(len_k)]
            ri = sum(ksai)/(len_k)
            r.append(ri)
        return r
    
    def houyancha(self,x0,x01):
        #后验差检验
        x0 = np.array(x0)
        x01 = np.array(x01)
        ksai=x0-x01
        
        s1 = abs(x0.std()) #与书上不一样
        s2 = abs(ksai.std())
        
        c = s2*1.0/s1
        
        lp = []
        ksai_m = ksai.mean()
        for k in ksai:
            delta = abs(k-ksai_m)
            if delta < 0.6745*s1:
                lp.append(delta)
        p = len(lp)*1.0/len(ksai)
        
        return (c,p)
    
    def relative_dev(self,a,b):
        #计算相对偏差
        return (b-a)*1.0/a
        
    
    

# if __name__ == "__main__":
#     rou = 0.5
#     raw_data = [[20,22,40,45,60,80],
#                  [30,35,55,60,70,90],
#                  [40,45,43,55,65,70]]
#     check = check()
# 
#     print check.guanlian(raw_data,rou)
        
        
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    