#!/usr/bin/env python
# -*- coding: UTF-8 -*-
'''
Created on 2013-7-12

@author: yanghl
'''
import math
import numpy as np


class GM(object):
    """
    灰色模型GM(1,1)与GM(1,n)
    """
    def Generate_values(self,x0):
        """
        计算原始数列的累加生成值
        """
        x0 = np.array(x0)
        x1 = np.zeros(len(x0))
        for i in range(len(x0)):
            if i ==0:
                x1[i]=x0[i]
            else:
                x1[i] = x1[i-1]+x0[i]
        return x1
    
    def GM_11(self,x0,n):
        """
        GM(1,1)模型
        """
        x1 = self.Generate_values(x0)
        
        #计算数据矩阵B和数据向量Yn
        x1 = np.array(x1)
        B = np.ones((len(x1)-1,2))
        for k in range(len(x1)-1):
            B[k,0] = -0.5*(x1[k]+x1[k+1])
            
        Yn = np.ones((len(x0)-1,1))
        for k in range(len(x1)-1):
            Yn[k,0]=x0[k+1]
        
        #计算方程参数a和u
        B1 = np.matrix(B)
        Yn1 = np.matrix(Yn)
        B_T = B1.getT() #getI获取矩阵的逆，getT获取矩阵的转置
        BTB = B_T*B1
        R = BTB.getI()*B_T*Yn1
        a = R[0,0]
        u = R[1,0]
#         print a,u
        
        #计算x11,x01
        number = len(x0) + n
        x11 = np.zeros(number)
        x01n = np.zeros(number)
        for k in range(number):
            x11[k]=(x0[0]-u*1.0/a)*math.exp(-a*k)+u/a
        
        for i in range(number):
            if i ==0:
                x01n[i]=x11[i]
            else:
                x01n[i] = x11[i]-x11[i-1]
                
        x01 = x01n[0:len(x0)] 
        x0n = x01n[len(x0):] #预测的数据,与书本不一样
        
        return (x01,x0n)  #x01为预测的初值，x01为未来预测值  
    
    def GM_1n(self,x10,x20):
        """
        GM(1,n)模型,在x20的影响下，对x10建立GM(1,2)预测模型
        """
        x11 = self.Generate_values(x10)
        x21 = self.Generate_values(x20)
#         print x11,x21
        
        #计算数据矩阵B和数据向量Yn
        x11 = np.array(x11)
        x21 = np.array(x21)
        B = np.ones((len(x11)-1,2))
        for k in range(len(x11)-1):
            B[k,0] = -0.5*(x11[k]+x11[k+1])
            B[k,1] = x21[k+1]
            
        Yn = np.ones((len(x10)-1,1))
        for k in range(len(x11)-1):
            Yn[k,0]=x10[k+1]
        
        #计算方程参数a和u
        B1 = np.matrix(B)
        Yn1 = np.matrix(Yn)
        B_T = B1.getT() #getI获取矩阵的逆，getT获取矩阵的转置
        BTB = B_T*B1
        R = BTB.getI()*B_T*Yn1
        a = R[0,0]
        b = R[1,0]
        
        #计算x11_u,x01_u
        number = len(x10)
        x21_u = np.zeros(number)
        x11_u = np.zeros(number)
        for k in range(number):
#             print x10[0],x21[k]
            x21_u[k]=(x10[0]-b*x21[k]/a)*math.exp(-a*k)+b/a*x21[k]
            
        for i in range(number):
            if i ==0:
                x11_u[i]=x21_u[i]
            else:
                x11_u[i] = x21_u[i]-x21_u[i-1]
        
        return (x11_u,x21_u)
                

        
    
    
                
    

if __name__ == "__main__":
    from check import check
    G = GM()
    check = check()
    
#     x0 = [20.47,21.39,22.04,23.36,24.06,26.51,27.98,30.40]
#     (x01,x0n)=G.GM_11(x0,5)
#     print x01,x0n
#     print check.houyancha(x0, x01)

    x10 = np.array([2.874,3.278,3.307,3.39,3.679])
    x20 = np.array([7.04,7.645,8.075,8.53,8.774])
    (x10_u,x20_u)=G.GM_1n(x10, x20)
    print check.relative_dev(x10, x10_u)
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
        